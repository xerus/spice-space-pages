Seamless applications / Rootless / standalone windows
###############################################################################

:slug: seamless-applications


Add the ability to run virtualized-os applications as normal
applications in seperate windows.

Bug: https://bugs.freedesktop.org/show_bug.cgi?id=39238

Current Status
++++++++++++++
- Targeted Release: none
- experimental patches and rpms https://copr.fedorainfracloud.org/coprs/lvenhoda/vd_agent-seamless-mode/

Description
+++++++++++
A rootless desktop allows to create a seamless desktop environment
between the client OS and the guest OS. This kind of functionality is
available in VMWare Unity view. Applications should behave as if they
were not in a seperate OS.

- applications are managed and are integrated in the client window-manager/shell
- show separate client windows for each applications
- copy & paste, dragging
- status bar
- application menu
- recents documents
- volume control
- to be continued...

References to other projects
++++++++++++++++++++++++++++
- we should probably patch/reuse seamlessrdp: http://rdesktop.svn.sourceforge.net/viewvc/rdesktop/seamlessrdp/trunk/
- launchy source code can help to figure out how to list start menu item
- see also http://msdn.microsoft.com/en-us/library/ms644977(v=vs.85).aspx
