HTML5 Client
###############################################################################

:slug: spice-html5
:modified: 2016-06-30 10:28

.. role:: bash(code)
   :language: bash

.. _TODO: http://cgit.freedesktop.org/spice/spice-html5/tree/TODO
.. _websockify: https://github.com/kanaka/websockify
.. _the git page: http://cgit.freedesktop.org/spice/spice-html5


Spice has a prototype Web client which runs entirely within a modern browser.  It is limited in function, a bit slow, and lacks support for many features of Spice (audio, video, agents just to name a few).

A complete TODO_ list is kept in the source code.

However, it is surprisingly functional, and certainly a useful proof of concept.

Requirements
++++++++++++

The browser must support binary WebSockets as well as the binaryType of arraybuffer. As of June, 2012, Firefox 12 and Chrome >= 18 both provided the required features.

Currently, you must also use a WebSocket proxy, as Spice has no built in support for the WebSocket protocol.  websockify_ works great.

You must, of course, also have a Spice server of some kind.  It has been tested primarily against Xspice, but it also works with qemu. However, it works better with linux guests than with Windows guests.

To Try It
+++++++++

The following steps should enable you to use the HTML5 Client:

#. (optional) Obtain the spice-html5 client.  See `the git page`_ for more details.
#. Start your spice server.  This is left as an exercise for the reader. For the purposes of this set of instructions, we imagine it is running on your localhost at port 5900.
#. Obtain websockify_
#. Start websockify :bash:`websockify/websockify.py 5959 localhost:5900`
#. Point your browser to spice.html and try it!

Be sure to give the hostname where websockify is running and the port number you provided when starting websockify.
